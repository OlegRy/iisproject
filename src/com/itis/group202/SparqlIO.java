package com.itis.group202;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SparqlIO {

    public static List<String> readFile(String fileName) throws IOException {
        List<String> questions = new ArrayList<String>();
        InputStreamReader streamReader = null;
        try {
            streamReader = new InputStreamReader(new FileInputStream(fileName));
            BufferedReader reader = new BufferedReader(streamReader);

            String line;
            while ((line = reader.readLine()) != null) {
                questions.add(line);
            }
        } catch (FileNotFoundException e) {
            System.err.println("Sorry, input file not found!");
            System.exit(1);
        } finally {
            streamReader.close();
        }

        return questions;
    }

    public static void writeToFile(List<String> sparqlQueries) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter("output.txt");

        for (String sparqlQuery : sparqlQueries) {
            writer.println(sparqlQuery);
        }

        writer.close();
    }
}
