package com.itis.group202.test;

import com.itis.group202.SparqlIO;

import java.io.IOException;

public class TestSparqlIO {

    public static void main(String[] args) throws IOException {

        SparqlIO.writeToFile(SparqlIO.readFile("input.txt"));
    }
}
